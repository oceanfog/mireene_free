<?php

$ar_dir = Array( 'folders' => Array(), 'files' => Array(), 'folderName' => "root" );

array_push($ar_dir['folders'], "hello");


echo $ar_dir;
echo "<br><br>";

print_r($ar_dir);
echo "<br><br>";

echo $ar_dir['folderName'];

echo "<br><br>";

$ar_dir['folderName'] = "hello";

echo $ar_dir['folderName'];
echo "<br><br>";

$ar_wrapper = Array('folderName' => "root");

print_r($ar_dir);
echo "<br><br>";

array_push($ar_wrapper, $ar_dir);

print_r($ar_wrapper);
echo "<br><br>";

echo json_encode( $ar_wrapper );
echo "<br><br>";

array_push($ar_wrapper, "hello");

echo json_encode( $ar_wrapper );
echo "<br><br>";

?>