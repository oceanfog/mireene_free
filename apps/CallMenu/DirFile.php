<?php
header("Content-Type: text/html; charset=utf-8");



class DirFile
{
	var $rootDir;
	var $folderName;
	
	public function __construct( $folderName ){
		$this->rootDir = $_SERVER['DOCUMENT_ROOT'];
		$this->folderName = $folderName;
	}
		
	function checkFolder( $folderName ){
		$dir = 	"./";
		
		if($folderName ==""){
			$dir = "./";	
		}else{
			$dir = "./$folderName/";
		}
		
		return $dir;
	}
	
	function getFoldersFiles( $folderName ){
			
		chdir($this->rootDir);
		
		$this->checkFolder( $folderName );
		
		$ar_dir = Array( 'folders' => Array(), 'files' => Array() );
		// Open a known directory, and proceed to read its contents
		if (is_dir($dir)) {
	    	if ($dh = opendir($dir)) {
		        while (($file = readdir($dh)) !== false) {
					if( filetype($dir.$file) == "dir"){
						//if ($file == "." || $file == "..") continue;				
						array_push($ar_dir['folders'], $file);
					}else{
						array_push($ar_dir['files'], $file);
					}
		        }
	        closedir($dh);
			sort($ar_dir['folders']);
			sort($ar_dir['files']);
			
	  		}
		}
		return $ar_dir;	
	} //getFoldersFiles
	
	
	function getFolders( $folderName ){
		$ar_folderNames = Array();

		chdir($this->rootDir);
		$dir = "./";
		$openedDir = opendir($dir);
		
		while( $file = readdir( $openedDir ) ){
			array_push($ar_folderNames, $file);
		}
		
		return $ar_folderNames;
	}
	
	function getFiles( $folderName ){
		$ar_fileNames = Array();
		
		chdir($this->rootDir);
		$dir = "./";
		$openedDir = opendir($dir);
		
		while( $file != readdir( $openedDir ) ){
			array_push($ar_fileNames, $file);
		}
		
		return $ar_fileNames;
	}
}

?>