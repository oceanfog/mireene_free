예제 소스는 Apache-php-mysql 환경에서 개발/TEST 되었습니다.
이 소스는 최적화된 소스가 아닙니다. 어디까지나 예제로서 이해하기 쉬운 형태로 제작 되었습니다.
소스는 2개의 PHP 파일로 구성 되어 있습니다.
 - step1.php : OAuth_Flow_Diagram.png의 A,B,C단계까지 입니다.
   * request_token, request_token_secret 을 받고 이것을 DB에 저장한 후 
    Authorize_URL로 Page를 이동(redirect)시킵니다.

   * step1.php 소스를 사용하려면 소스 내에 다음 변수에 여러분이 직접 적절한 값을 입력 해야 합니다.
			$strMysqlHost : token을 저장할 mysql db host 정보
			$strMysqlID : token을 저장할 mysql db 접속 ID
			$strMysqlPassword : token을 저장할 mysql db 접속 암호
			$oauth_consumer_key : http://devsquare.nate.com 에서 발급받은 건슈머키
			$oauth_consumer_key_secret : http://devsquare.nate.com 에서 발급받은 컨슈머 시크릿
			$oauth_callback : step2.php 파일의 주소
 

- step2.php : OAuth_Flow_Diagram.png의 D,E,F,G 단계까지 입니다.
* step1.php파일에서 request_token을 받을 때 oauth_callback의 값은 이 파일이 존재하는
 여러분의 사이트 주소를 입력해두었다면 Authorize_URL 이동 후 사용자가 Nate 로그인을 성공하면
 Nate는 step2.php를 호출합니다. 이때 전달 파라메터로 oauth_token, oauth_verifier 가 포함됩니다.

* 전달 받은 oauth_token은 Authorize_URL을 호출할 때 전달한 request_token값입니다. step1.php에서
   request_token, request_token_secret 을 DB에 저장 해 두었기 때문에 전달받은 request_token을 key
 DB에서 request_token_secret을 select 합니다.

* 이후 request_token, request_token_secret, oauth_verifier 를 이용하여 Access token을 요청합니다.

* access_token, access_token_secret을 성공적으로 확보하면 이를 이용하여 openAPI를 호출, 
   보호된 자원을 조회할 수 있습니다.

* step2.php 소스를 사용하려면 소스 내에 다음 변수에 여러분이 직접 적절한 값을 입력 해야 합니다.
			$strMysqlHost : token을 저장할 mysql db host 정보
			$strMysqlID : token을 저장할 mysql db 접속 ID
			$strMysqlPassword : token을 저장할 mysql db 접속 암호
			$oauth_consumer_key : http://devsquare.nate.com 에서 발급받은 건슈머키
			$oauth_consumer_key_secret : http://devsquare.nate.com 에서 발급받은 컨슈머 시크릿
 


 - 자세한 사항은 소스와 소스상의 주석을 참조 하세요.

Mysql_DDL.txt 은 Sample 소스에서 사용된 mysql DB의 Table 생성 Query가 담겨있습니다.