<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd"><html dir="ltr" lang="en"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"><title>Alert Page</title><link rel="stylesheet" href="../csstg.css" type="text/css"><style>
/* ---- ( iTunes CSS ) ---- */
table { 
	font: 80% Verdana, Arial, Helvetica, sans-serif;
	color: #000;
	text-align: left;
	border-collapse: collapse;
	border: 1px solid #666666;
	border-top: none;	
}
table a {
	text-decoration: underline;
}
table a:visited {
	text-decoration: none;
}
tr.odd {
	background-color: #ebf3ff;
}
tr a {
	color: #000000;
}
tr:hover a {
	color: #ffffff;
}
tr:hover, tr.odd:hover {
	background-color: #3d80df;
	color: #ffffff;
}
caption {
	height: 45px;
	line-height: 44px;
	color: #60634E;
	font-weight: bold;
	text-align: center;
	width: 824px;
	margin: 0;
	padding: 0;
	margin-left: -1px;
	background: #ffffff url(http://atnsts.iptime.org:8080/image/captop.jpg) repeat-x;
	background-position: 50% top;
	border-left: 2px solid #616161;
	border-right: 2px solid #616161;
}
thead th {
	font-size: 10px;
	color: #000;
	background: #ffffff url(http://atnsts.iptime.org:8080/image/tbar.gif) repeat-x;
	height: 20px;
	text-align: center;
}
thead th:hover {
	background: #ffffff url(http://atnsts.iptime.org:8080/image/tbov.gif) repeat-x;
	vertical-align: middle;	
}
tr {
	vertical-align: top;
}
tr,th,td {
	padding: .75em;
	font-size: 10px;
}
td {
	border-left: 1px solid #dadada;
}
tfoot tr {
	background: #fff url(http://atnsts.iptime.org:8080/image/bbar.gif) repeat-x;
}
tfoot td, tfoot th{
	color: #000;
	border: 0px;
}
</style>


<table width="1024" summary="Submitted table designs">

<!--
Borard Title Image Bar
<caption>Alert Monitor</caption> 
-->

	  <thead>
          <tr>
          <th width="30" scope="col">&nbsp;</th>
          <th width="189" scope="col">AlertName</th>
          <th width="107" scope="col"> DateTime</th>
          <th width="107" scope="col">ServerName</th>
          <th width="454" scope="col">AlertShortDescription</th>
          <th width="109" scope="col"> Summary</th>
          </tr>
      </thead>
          
          
<!--  <tfoot>
          <tr><th scope="row">Total</th>
          <td colspan="5">xx Alerts</td></tr>
      </tfoot> -->

</TABLe>          
