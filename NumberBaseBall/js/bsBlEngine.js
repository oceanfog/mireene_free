var gv_baseBallNumber;
var gv_ar_input;
var gv_CL_BsbNum;
var gv_CL_ST_Score;

function CL_ST_Score( domObjectName ){
	this.tryTimes = 0;
	this.printTryTimes(domObjectName);
}

CL_ST_Score.prototype.addTryTimes = function(){
	this.tryTimes++;
}

CL_ST_Score.prototype.printTryTimes = function( domObjectName ){
	jQuery(domObjectName).val( this.tryTimes );
}

CL_ST_Score.prototype.getTryTimes = function( domObjectName ){
	return this.tryTimes;
}


function startGame(){
	gv_CL_BsbNum = new CL_BsbNum( 3 );
	gv_CL_ST_Score = new CL_ST_Score( "#tf_tryTimes" );
	
	//console.log("ar_bsbNum:" + gv_CL_BsbNum.ar_bsbNum );
	alert("The game has started");
}

function runGame(targetDiv, tf_score){
	var inputValue;
	
	//console.log(gv_baseBallNumber.charAt(0) );
	if( gv_CL_BsbNum == null || gv_CL_BsbNum =="" ) {
		startGame();
	}
	
	if ( isEmpty( jQuery(tf_answer).val() ) == 1 ){
		//if tf is empty then generate 3 random length value.
		alert("tf_answer has empty");
		jQuery(tf_answer).val( Math.floor( gv_CL_BsbNum.getRndNum(100, 999 ) ) );
	}
	
	gv_CL_ST_Score.addTryTimes();
	gv_CL_ST_Score.printTryTimes( tf_score );
	inputValue = getSeveralNumber( jQuery(tf_answer).val() ) ;
	ar_answerStatusLog = getAnswerStatusLog( gv_CL_BsbNum.ar_bsbNum, inputValue )
	printAnswerLog(targetDiv, inputValue, ar_answerStatusLog );
	isRight( getAnswerStatusLog( gv_CL_BsbNum.ar_bsbNum, inputValue ) );
	jQuery(tf_answer).val("");
}

function endGame(){
	changeDOMBgColor( "#article03_01", "#19A347" );
	alert("Congratulation! You have take the right answer! in " + gv_CL_ST_Score.getTryTimes() + "times." );
	clearObject( "#article03_01" );
	changeDOMBgColor( "#article03_01", "#E6EEF0" );
	gv_CL_BsbNum = "";
}


function CL_BsbNum( numCnt ){
	this.ar_bsbNum = [];
	this.setBaseBallNumber( numCnt );
}

CL_BsbNum.prototype.setBaseBallNumber = function( num_ea ){
	this.ar_bsbNum = this.genBsbNum( num_ea );
}


CL_BsbNum.prototype.genBsbNum = function( num_ea ){
	var rndNum;
	var ar_genNum = [];
	
	if( ar_genNum.length == 0){
		ar_genNum.push( this.getRndNum(1, 9) );
	}
	
	//generate rndNum
	//check rndNum
	//is ok add rndNum nor go to first
	
	for ( arCnt = 1 ; arCnt < num_ea ; arCnt++ ){
		rndNum = this.getRndNum(1, 9);
		
		//check logic
		if( this.isDuplicated( ar_genNum, rndNum) == 0 ){
			ar_genNum.push( rndNum );
		}
		else{
			arCnt--
		}
	}//for
	
	return ar_genNum;
}



CL_BsbNum.prototype.getRndNum = function( startNum, endNum ){
	var rndNum;
	
	rndNum = Math.floor( (Math.random()*(endNum - startNum) ) + startNum );
	
	return rndNum;
}

CL_BsbNum.prototype.isDuplicated = function( ar_targetAr, generatedNum ){
	var dupCnt = 0;
	
	for ( i = 0 ; i < ar_targetAr.length ; i++ ){
		if( ar_targetAr[i] == generatedNum ){
			dupCnt++;
			break;
		}else{
		}
	}
	
	return dupCnt;
}

function printAnswerLog( s_targetDOMObj, inputValue, answerData ){
	jQuery( s_targetDOMObj ).append( "<br>" + gv_CL_ST_Score.getTryTimes() + " try input numebr: " + inputValue );
	jQuery( s_targetDOMObj ).append( "_______" + "status: " + answerData );
}


function getSeveralNumber( pa_number ){
	var ar_severalNum = [];
	
	fstNum = parseInt( pa_number / 100 );
	secNum = parseInt( pa_number % 100 / 10 );
	trdNum = pa_number % 100 % 10;
	
	ar_severalNum.push(fstNum);
	ar_severalNum.push(secNum);
	ar_severalNum.push(trdNum);
	
	return ar_severalNum;
}


function getAnswerStatusLog( ar_answer, ar_input ){
	//all number and location match.
	//all number match
	//several location and number match
	//several number only match
	//123 : 324
	//location, number
	var ar_procResult = [];
	var cnt_numMatchs;
	var cnt_numLocMatchs;
	
	cnt_numMatchs = 0;
	cnt_numLocMatchs = 0;
	
	for (loc_answer = 0 ; loc_answer < ar_answer.length ; loc_answer++){
		for ( loc_input = 0 ; loc_input < ar_answer.length ; loc_input++){
			if( ar_answer[loc_answer] == ar_input[loc_input] && loc_answer == loc_input){
				
				cnt_numLocMatchs++;
				
			}else if ( ar_answer[loc_answer] == ar_input[loc_input] && loc_answer != loc_input){
				
				cnt_numMatchs++;
				
			}
		}
	}
	
	ar_procResult.push(cnt_numLocMatchs);
	ar_procResult.push(cnt_numMatchs);
	
	return ar_procResult;
}



function isRight( ar_result ){
	if( ar_result[0] == 3){
		endGame();
	}
}

function changeDOMBgColor( nm_s_DOMObject, s_colorCode ){
	jQuery( nm_s_DOMObject ).css({ "background-color": s_colorCode });
}

function setAnswer( ar_answer ){
	gv_ar_answer = ar_answer;
	
}

function isEmpty( pa_object ){
	if( pa_object == null || pa_object == "" ){
		return 1;
	}else{
		return 0;
	}
}

function isAnswer( ar_answer ){
	//if gv_ar_answer has not generated then return 0 else generated return 1
	if( isEmpty( gv_ar_answer ) == 0 ){
		if( ar_answer[0] != null && ar_answer[1] != null && ar_answer[2] != null ){
			return 1;					
		}else{
			return 0;
		}
	}else{

	}
}


function clearObject( nm_s_targetObject ){
	jQuery( nm_s_targetObject ).empty();
}

function clearBsbNum(){
	gv_CL_BsbNum = "";
}
