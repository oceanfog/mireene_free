var authorization;

var t = setInterval(function(){
	connectedTime = gv_pageOption['openedTime'];
	nowTime = new Date() - 1;
	connectingTime = nowTime - connectedTime;
	
	if ( connectingTime > 1200000){
		alert("20 minuts has passed. The Session has closed.");
		initBody( ar_sc_login, ar_css_fb51 );
	}
}, 30000 );

var gv_pageOption = {
		"skin":0,
		"name":"krk",
		"rootDir":"http://oceanfog3.mireene.com/DrawingWebPage/WebContent/",
		"domObj":"",
		"authorization":"no",
		"openedTime":null,
		"nowPageObjNam":null
			};

var bgImgNam = [
               "Cartoon-Vector-Surf-Wallpaper-1024x768.jpg",
               "cubes-bleu-wallpaper-1024x768.jpg",
               "Gucci_1_Fashion_Wallpaper_1024X768_Wallpaper.jpg",
               "minimal-cartoon-clouds-blue-background-1024x768.jpg",
               "winter-background-wallpapers_7447_1024x768.jpg",
               "spring-flowers-background-1024x768.jpg",
               "11848-red-hd-wallpaper-background.jpg"
               ];

function initBody( ar_frame, p_ar_css ){
	jQuery("html body").empty();
	gv_pageOption['openedTime'] = (new Date()) - 1 ;

	var cDOMObj = new CDOMObj( );
	jQuery( ar_frame['objects'] ).each(function(key, val){
		cDOMObj.setDOMObjProperties( val );
		cDOMObj.putDOMObj( );
	});
	
	setCssEachObject( p_ar_css );
	eventHandlerInitializer();
	
	if( ar_frame['objNam'] = "docList"){
		parsingData("#section_docList #article_03 #tb_01", docData_bkpf);
	}
}

function parsingData( tarObj, jsonData){
	var data = jsonData;
	
	for ( i = 0 ; i < 15 ; i++ ){
		jQuery(tarObj).append(
				"<tr>"+
				"<td>" + data[i]['bukrs'] + "</td>" +
				"<td>" + data[i]['belnr'] + "</td>" +
				"<td>" + data[i]['gjahr'] + "</td>" +
				"<td>" + data[i]['gjahr'] + "</td>" +
				"<td>" + data[i]['gjahr'] + "</td>" +
				"<td>" + data[i]['gjahr'] + "</td>" +
				"<td>" + data[i]['gjahr'] + "</td>" +
				"<td>" + data[i]['gjahr'] + "</td>" +
				"</tr>"
				);
	}
}


function CDOMObj( ){
	var p_DOMObjTyp;
	var p_targetDOMObjId;
	var p_DOMObjId;
	var p_contents;
	var p_inputType;
	var p_url;
	var p_class;
	var p_colspan;
}

CDOMObj.prototype.putDOMObj = function( ){
	var data;
	
	switch( this.p_DOMObjTyp ){
		case "input":
			data = 	"<" + this.p_DOMObjTyp +
				" id='"+this.p_DOMObjId + 
				"' type='"+ this.p_inputType +
				"' value='"+ this.p_value +
				"'>" + this.p_contents +
				"</" + this.p_DOMObjTyp +">";
			break;
			
		case "a":
			data = "<" + "a"+" id='"+this.p_DOMObjId +
				"' href='"+ p_url +"'>" + this.p_contents +
				"</" + this.p_DOMObjTyp +">";
			break;
		
		case "button":
			data= "<" + this.p_DOMObjTyp +
			" id='"+this.p_DOMObjId +
			"' class='"+ this.p_class +"'>" + this.p_contents +"</" + this.p_DOMObjTyp +">";
		break;
		
		case "td":
			data = "<" + this.p_DOMObjTyp +
			" id='"+this.p_DOMObjId +
			"' colspan='"+this.p_colspan +
			"' class='"+ this.p_class +"'>" + this.p_contents +
			"</" + this.p_DOMObjTyp +">";
		break;
		
		case "img":
			data = "<" + this.p_DOMObjTyp +
			" src='"+this.p_url +
			"' id='"+this.p_DOMObjId +
			"' colspan='"+this.p_colspan +
			"' class='"+ this.p_class +"'>"
		break;
		
		default:
			data = "<" + this.p_DOMObjTyp+
			" id='"+this.p_DOMObjId +
			"' class='"+ this.p_class + "'>" + this.p_contents +
			"</" + this.p_DOMObjTyp +">";
	}

	jQuery(this.p_targetDOMObjId).append(data);
	this.emptyDOMObjProperties();
	
};


CDOMObj.prototype.emptyDOMObjProperties = function( ){
	this.setDOMObjProperties( {"type":"", "id":"", "tarObj":"", "contents":"", "inputType":"", "class":""} );
};

CDOMObj.prototype.setDOMObjProperties = function( ar_obj ){
	this.p_DOMObjTyp		= ar_obj['type'];
	this.p_targetDOMObjId	= ar_obj['tarObj'];
	this.p_DOMObjId			= ar_obj['id'];
	this.p_contents			= ar_obj['contents'];
	this.p_inputType		= ar_obj['inputType'];
	this.p_value			= ar_obj['value'];
	this.p_class			= ar_obj['class'];
	this.p_colspan			= ar_obj['colspan'];
	this.p_url				= ar_obj['url'];
	
	
};


function CAuthorization (){
	var stat_authorization = "no";
}

CAuthorization.prototype.getAuthStat = function(){
	return this.stat_authorization;
};


function setHtml( p_location, p_callType ){
	var depDat;
	
	jQuery.ajax({
		async: false,
		global: false,
	    type: "get",
	    contentType: "application/x-www-form-urlencoded; charset=UTF-8",
	    url: gv_pageOption['rootDir']+p_location,
	    dataType: p_callType,
	    timeout:  30000,
	    success: function(data){
	    	depDat = data;
	    }
	});
	
	return depDat;
}



function CDocument(){
	var docObj01;
}

CDocument.prototype.setDocObj = function(){
	docObj01 = {
			"document_header":{
				"document_number":"7200000001",
				"posting_date":"2013-11-19",
				"document_date":"2013-11-19",
				"currency":"EUR"
				},
			"document_line_item01":{
				"account_number":"113100",
				"debit_credit":"Credit",
				"amount":"10000",
				"coObj":"ATSG"
				},
			"document_line_item02":{
				"account_number":"400000",
				"debit_credit":"Debit",
				"amount":"10000",
				"coObj":"ATSG"
				}
		};
};



function setCssEachObject( ar_objSet ){
	for ( var key in ar_objSet){
		jQuery( key ).css(ar_objSet[key]);
	}
}



/**********************************************************
 * object records
 * 
 ********************************************************/


var nav_data =
	"<div align='right' style='margin-top:10px; background-color='#ffffff';'>"+
	"user info"+
	"<label>id:</label>"+
	"<input id='nav_tf_tarObj' type='text' value=''></input>"+
	"<label>department:</label>"+
	"<input id='nav_tf_colVal' type='text' value=''></input>"+
	"<button id='nav_bu_colSubmit' class='bu_css2'>login</button>"+
	"<button id='nav_bu_colSubmit' class='bu_css2'>logout</button>"+
	"<button id='nav_bu_background' class='bu_css2'>background</button>"+
	"</div>";

var menu_data=
		"<div style='background-color:#; margin-left:15px; margin-top:5px;'>"+
		"<span id='spn_home' style='pedding:0px; font-size:25px; font-weight:bold; border:0px solid #111111'>PLAS</span><br>"+
		"<span style='pedding:0px; font-size:smaller; border:0px solid #111111'>Paper Less Accounting System한</span>"+
		"</div>"+
		"<br><br><br><br><br><br>"+
		"<div style='width:187px; height:612px; border-radius:2px; background-color:#ffffff; opacity:0.99'>"+
		menu+
		"</div>";

var ar_sc_doc_list_obj = [
                          //{"type":"div", "id":"div_buttonBox", "tarObj":"#id_body", "contents":setHtml( "./top_menu.html", "html" )},
                          {"type":"div", "id":"div_wrapper", "tarObj":"#id_body", "contents":""},
                          
                          {"type":"aside", "id":"aside_aside", "tarObj":"#div_wrapper", "contents":menu_data},
                          {"type":"nav", "id":"nav_nav", "tarObj":"#div_wrapper", "contents":nav_data},
                          {"type":"section", "id":"section_docList", "tarObj":"#div_wrapper", "contents":"Document List<br>"},
                           
                           
                          {"type":"article", "id":"article_02", "tarObj":"#section_docList", "contents":"article_02<br>"},
                          {"type":"label", "id":"lab_01", "tarObj":"#article_02", "contents":"거래처", "inputType":""},
                          {"type":"td", "id":"td_01", "tarObj":"#article_02", "contents":"", "value":""},
                          {"type":"input", "id":"tf_01", "tarObj":"#article_02 #td_01", "contents":"", "inputType":"text", "value":""},
                          {"type":"button", "id":"bu_doCheck", "tarObj":"#article_02", "contents":"Ch", "class":"bu_css2"},
                          {"type":"button", "id":"bu_doCheck", "tarObj":"#article_02", "contents":"Ch", "class":"bu_css2"},
                          {"type":"label", "id":"lab_01", "tarObj":"#article_02", "contents":"진행상태", "inputType":""},
                          {"type":"select", "id":"lab_01", "tarObj":"#article_02", "contents":"전체", "inputType":""},
                          {"type":"label", "id":"lab_01", "tarObj":"#article_02", "contents":"전기일자", "inputType":""},
                          {"type":"td", "id":"td_02", "tarObj":"#article_02", "contents":"", "value":""},
                          {"type":"input", "id":"tf_01", "tarObj":"#article_02 #td_02", "contents":"", "inputType":"text", "value":""},
                           
                          {"type":"td", "id":"td_03", "tarObj":"#article_02", "contents":"", "value":""},
                          {"type":"input", "id":"tf_01", "tarObj":"#article_02 #td_03", "contents":"", "inputType":"text", "value":""},
                          {"type":"button", "id":"bu_search", "tarObj":"#article_02", "contents":"Search", "class":"bu_css2"},
                           
                          {"type":"article", "id":"article_03", "tarObj":"#section_docList", "contents":"article_03<br>"},
	                          {"type":"table", "id":"tb_01", "tarObj":"#article_03", "contents":"", "inputType":"", "class":"null"},
	                          {"type":"tr", "id":"tr_01", "tarObj":"#tb_01", "contents":"", "inputType":""},
	                          	{"type":"th", "id":"td_01", "tarObj":"#tr_01", "contents":"선택", "inputType":"", "class":"" },
	                          	{"type":"th", "id":"td_02", "tarObj":"#tr_01", "contents":"번호", "inputType":"", "class":""},
	                          	{"type":"th", "id":"td_03", "tarObj":"#tr_01", "contents":"계정", "inputType":"", "class":"" },
	                          	{"type":"th", "id":"td_04", "tarObj":"#tr_01", "contents":"거래처", "inputType":"", "class":""},
	                          	{"type":"th", "id":"td_05", "tarObj":"#tr_01", "contents":"Cost Object", "inputType":"", "class":""},
	                          	{"type":"th", "id":"td_06", "tarObj":"#tr_01", "contents":"차변 금액(KRW)", "inputType":"", "class":""},
	                          	{"type":"th", "id":"td_07", "tarObj":"#tr_01", "contents":"대변 금액(KRW)", "inputType":"", "class":""},
	                          	{"type":"th", "id":"td_08", "tarObj":"#tr_01", "contents":"세부 사항", "inputType":"", "class":""},
                           
                          {"type":"article", "id":"article_04", "tarObj":"#section_docList", "contents":"article_03<br>"},
                          {"type":"button", "id":"bu_search", "tarObj":"#article_04", "contents":"신규", "class":"bu_css2"},
                          
                           ];
var ar_sc_doc_list = {"objNam":"docList", "objects":ar_sc_doc_list_obj};




var ar_color_sap2 ={"header":{ "background-color":"#11282D"},
					"nav":{ "background-color":"#DEC9AC"},
					"aside":{ "background-color":"#CCA18B"},
					"section":{ "background-color":"#A5C4BB"},
					"article":{ "background-color":"#11282D"},
					"input[type=text]":{ "background-color":"#ffffff"}
					};


var ar_sc_home_obj= [
                  {"type":"div", "id":"div_wrapper", "tarObj":"#id_body", "contents":""},
                  {"type":"aside", "id":"aside_aside", "tarObj":"#div_wrapper", "contents":menu_data},
                  {"type":"nav", "id":"nav_nav", "tarObj":"#div_wrapper", "contents":nav_data},
                  
                  {"type":"section", "id":"section_home", "tarObj":"#div_wrapper", "contents":""},
                 
                  {"type":"article", "id":"article_01", "tarObj":"#section_home", "contents":""},
                  	{"type":"div", "id":"div_01", "tarObj":"#article_01", "contents":""},
                  		{"type":"div", "id":"div_subject", "tarObj":"#div_01", "contents":""},
                  		{"type":"div", "id":"div_square_01", "tarObj":"#div_01", "contents":""},
//                  			{"type":"img", "id":"img_01", "tarObj":"#div_01 #div_square_01", "contents":"", "url":"./images/home/credit_card.png"},
                  		{"type":"div", "id":"div_square_02", "tarObj":"#div_01", "contents":""},
                  
                  	{"type":"div", "id":"div_02", "tarObj":"#article_01", "contents":""},
	                  	{"type":"div", "id":"div_subject", "tarObj":"#div_02", "contents":""},
	              		{"type":"div", "id":"div_square_01", "tarObj":"#div_02", "contents":""},
	              			{"type":"div", "id":"div_status_01", "tarObj":"#div_02 #div_square_01", "contents":""},
	              				{"type":"button", "id":"bu_status_01", "tarObj":"#div_02 #div_square_01 #div_status_01", "contents":"전자증빙전표 현황"},
	              				{"type":"p", "id":"p_status_01", "tarObj":"#div_02 #div_square_01 #div_status_01", "contents":"미처리(법인카드) 0건"},
	              				{"type":"p", "id":"p_status_01", "tarObj":"#div_02 #div_square_01 #div_status_01", "contents":"결재요청 0건"},
	              				{"type":"p", "id":"p_status_01", "tarObj":"#div_02 #div_square_01 #div_status_01", "contents":"결재진행중 0건"},
	              				{"type":"p", "id":"p_status_01", "tarObj":"#div_02 #div_square_01 #div_status_01", "contents":"반려 0건"},
	              			{"type":"div", "id":"div_status_02", "tarObj":"#div_02 #div_square_01", "contents":""},
	              				{"type":"button", "id":"bu_status_01", "tarObj":"#div_02 #div_square_01 #div_status_02", "contents":"실물증빙전표 현황"},
	              				{"type":"p", "id":"p_status_01", "tarObj":"#div_02 #div_square_01 #div_status_02", "contents":"결재요청 0건"},
	              				{"type":"p", "id":"p_status_01", "tarObj":"#div_02 #div_square_01 #div_status_02", "contents":"결재진행중 0건"},
	              				{"type":"p", "id":"p_status_01", "tarObj":"#div_02 #div_square_01 #div_status_02", "contents":"반려 0건"},
	              			{"type":"div", "id":"div_status_03", "tarObj":"#div_02 #div_square_01", "contents":""},
	              				{"type":"button", "id":"bu_status_01", "tarObj":"#div_02 #div_square_01 #div_status_03", "contents":"법인카드 발급신청 현황"},
	              				{"type":"p", "id":"p_status_01", "tarObj":"#div_02 #div_square_01 #div_status_03", "contents":"결재요청 0건"},
	              				{"type":"p", "id":"p_status_01", "tarObj":"#div_02 #div_square_01 #div_status_03", "contents":"결재진행중 0건"},
	              				{"type":"p", "id":"p_status_01", "tarObj":"#div_02 #div_square_01 #div_status_03", "contents":"반려 0건"},

                  	
                  {"type":"article", "id":"article_02", "tarObj":"#section_home", "contents":""},
                  	
          			{"type":"div", "id":"div_square_01", "tarObj":"#article_02", "contents":""},
          				{"type":"img", "id":"img_01", "tarObj":"#article_02 #div_square_01", "contents":"", "url":"./images/home/icon_notice.gif"},
          				{"type":"div", "id":"div_status_01", "tarObj":"#article_02 #div_square_01", "contents":"Notice"},
          				{"type":"div", "id":"div_status_02", "tarObj":"#article_02 #div_square_01", "contents":""},
          					
          					
      				{"type":"div", "id":"div_square_02", "tarObj":"#article_02", "contents":""},
      					{"type":"img", "id":"img_01", "tarObj":"#article_02 #div_square_02", "contents":"", "url":"./images/home/icon_qna.gif"},
      					{"type":"div", "id":"div_status_01", "tarObj":"#article_02 #div_square_02", "contents":"Q & A"},
      					{"type":"div", "id":"div_status_02", "tarObj":"#article_02 #div_square_02", "contents":""}
                  ];
var ar_sc_home = {"objects":ar_sc_home_obj};






var sc_doc_posted_obj = [
                      {"type":"div", "id":"div_wrapper", "tarObj":"#id_body", "contents":""},
                      {"type":"nav", "id":"nav_nav", "tarObj":"#div_wrapper", "contents":nav_data},
                      {"type":"aside", "id":"aside_aside", "tarObj":"#div_wrapper", "contents":menu_data},
                      {"type":"section", "id":"section_section", "tarObj":"#div_wrapper", "contents":"document has posted"}
                     ];
var sc_doc_posted = {"objects":sc_doc_posted_obj};


var ar_sc_doc_fb50_obj = [
               {"type":"div", "id":"div_wrapper", "tarObj":"#id_body", "contents":""},
               
               {"type":"aside", "id":"aside_aside", "tarObj":"#div_wrapper", "contents":menu_data },
               {"type":"nav", "id":"nav_nav", "tarObj":"#div_wrapper", "contents":nav_data},
               {"type":"section", "id":"section_section", "tarObj":"#div_wrapper", "contents":""},
               {"type":"article", "id":"article_01", "tarObj":"#section_section", "contents":"Document Header<br>"},
               {"type":"article", "id":"article_02", "tarObj":"#section_section", "contents":"Document Line Item<br>"},
               {"type":"article", "id":"article_03", "tarObj":"#section_section", "contents":""},
               {"type":"article", "id":"article_04", "tarObj":"#section_section", "contents":"Object Viewer"},
               {"type":"article", "id":"article_05", "tarObj":"#section_section", "contents":"Debugging Screen"},
               
               {"type":"label", "id":"lab_01", "tarObj":"#article_01", "contents":"Posting Date", "inputType":""},
               {"type":"input", "id":"tf_01", "tarObj":"#article_01", "contents":"", "inputType":"text", "value":"2013.11.19"},
               {"type":"label", "id":"lab_02", "tarObj":"#article_01", "contents":"Document Date", "inputType":""},
               {"type":"input", "id":"tf_02", "tarObj":"#article_01", "contents":"", "inputType":"text", "value":"2013.11.19"},
               {"type":"label", "id":"lab_03", "tarObj":"#article_01", "contents":"Currency", "inputType":""},
               {"type":"input", "id":"tf_03", "tarObj":"#article_01", "contents":"", "inputType":"text", "value":"EUR"},
               
               {"type":"br", "id":"", "tarObj":"#article_01", "contents":"", "inputType":"text", "value":"EUR"},
               
               {"type":"label", "id":"lab_03", "tarObj":"#article_01", "contents":"Company Code", "inputType":""},
               {"type":"input", "id":"tf_03", "tarObj":"#article_01", "contents":"", "inputType":"text", "value":"1000"},
               
               {"type":"label", "id":"lab_04", "tarObj":"#article_01", "contents":"Document Number", "inputType":""},
               {"type":"input", "id":"tf_04", "tarObj":"#article_01", "contents":"", "inputType":"text", "value":"72000000001"},
               
               {"type":"div", "id":"div_lineItem_01", "tarObj":"#article_02", "contents":""},
               {"type":"input", "id":"tf_linItm_01", "tarObj":"#div_lineItem_01", "contents":"", "inputType":"text", "value":"113100"},
               {"type":"input", "id":"tf_linItm_02", "tarObj":"#div_lineItem_01", "contents":"", "inputType":"text", "value":"Dte Bank"},
               {"type":"input", "id":"tf_linItm_03", "tarObj":"#div_lineItem_01", "contents":"", "inputType":"text", "value":"Credit"},
               {"type":"input", "id":"tf_linItm_04", "tarObj":"#div_lineItem_01", "contents":"", "inputType":"text", "value":"10000"},
               {"type":"input", "id":"tf_linItm_05", "tarObj":"#div_lineItem_01", "contents":"", "inputType":"text", "value":"ATSG"},
               
               {"type":"div", "id":"div_lineItem_02", "tarObj":"#article_02", "contents":""},
               {"type":"input", "id":"tf_03", "tarObj":"#div_lineItem_02", "contents":"", "inputType":"text", "value":"400000"},
               {"type":"input", "id":"tf_03", "tarObj":"#div_lineItem_02", "contents":"", "inputType":"text", "value":"Consumptn, raw mat.1"},
               {"type":"input", "id":"tf_03", "tarObj":"#div_lineItem_02", "contents":"", "inputType":"text", "value":"Debit"},
               {"type":"input", "id":"tf_03", "tarObj":"#div_lineItem_02", "contents":"", "inputType":"text", "value":"10000"},
               {"type":"input", "id":"tf_03", "tarObj":"#div_lineItem_02", "contents":"", "inputType":"text", "value":"ATSG"},
               
               {"type":"button", "id":"bu_submit_gl", "tarObj":"#article_03", "contents":"Posting", "class":"bu_css2"},
               {"type":"button", "id":"button_bu05", "tarObj":"#article_03", "contents":"clear", "class":"bu_css2"},
               {"type":"button", "id":"bu_onInputProcessing", "tarObj":"#article_03", "contents":"OnInput", "class":"bu_css2"}
              ];
var ar_sc_doc_fb50 = {"objects":ar_sc_doc_fb50_obj};




var ar_sc_doc_fb51_obj=	[
                      {"type":"div", "id":"div_wrapper", "tarObj":"#id_body", "contents":""},
                      {"type":"aside", "id":"aside_aside", "tarObj":"#div_wrapper", "contents":menu_data },
                      {"type":"nav", "id":"nav_nav", "tarObj":"#div_wrapper", "contents":nav_data},
                      {"type":"section", "id":"section_section", "tarObj":"#div_wrapper", "contents":""},
                      {"type":"article", "id":"article_01", "tarObj":"#section_section", "contents":"Document Header<br>"},
                      {"type":"article", "id":"article_02", "tarObj":"#section_section", "contents":"Document Line Item<br>"},
                      {"type":"article", "id":"article_03", "tarObj":"#section_section", "contents":""},
                      {"type":"article", "id":"article_04", "tarObj":"#section_section", "contents":"Object Viewer"},
                      {"type":"article", "id":"article_05", "tarObj":"#section_section", "contents":"Debugging Screen"},
                      
                      
                      {"type":"table", "id":"tb_01", "tarObj":"#article_01", "contents":"", "inputType":""},
                      {"type":"tr", "id":"tr_01", "tarObj":"#tb_01", "contents":"", "inputType":""},
                      	{"type":"td", "id":"td_01", "tarObj":"#tr_01", "contents":"req.No", "inputType":"", "class":"cl_td_nam" },
                      	{"type":"td", "id":"td_01", "tarObj":"#tr_01", "contents":"", "inputType":"", "class":"cl_td_val"},
                      	{"type":"td", "id":"td_01", "tarObj":"#tr_01", "contents":"doc.No", "inputType":"", "class":"cl_td_nam" },
                      	{"type":"td", "id":"td_01", "tarObj":"#tr_01", "contents":"", "inputType":"", "class":"cl_td_val"},
                      {"type":"tr", "id":"tr_02", "tarObj":"#tb_01", "contents":"", "inputType":""},
	                  	{"type":"td", "id":"td_01", "tarObj":"#tr_02", "contents":"posting Dat", "inputType":"", "class":"cl_td_nam" },
	                  	{"type":"td", "id":"td_02", "tarObj":"#tr_02", "contents":"", "inputType":""},
	                  	{"type":"td", "id":"td_inner_01", "tarObj":"#td_02", "contents":"", "inputType":"", "class":"cl_td_inner"},
	                  	{"type":"input", "id":"tf_01", "tarObj":"#td_inner_01", "contents":"", "inputType":"text", "value":"2013.11.19"},
	                  	{"type":"td", "id":"td_01", "tarObj":"#tr_02", "contents":"doc Dat", "inputType":"", "class":"cl_td_nam" },
	                  	{"type":"td", "id":"td_01", "tarObj":"#tr_02", "contents":"", "inputType":""},
                      
	                  {"type":"tr", "id":"tr_03", "tarObj":"#tb_01", "contents":"", "inputType":""},
		              	{"type":"td", "id":"td_01", "tarObj":"#tr_03", "contents":"Currency", "inputType":"", "class":"cl_td_nam" },
	                  	{"type":"td", "id":"td_01", "tarObj":"#tr_03", "contents":"", "inputType":"", "colspan":3},
		                           
                      {"type":"tr", "id":"tr_04", "tarObj":"#tb_01", "contents":"", "inputType":""},
                      	{"type":"td", "id":"td_01", "tarObj":"#tr_04", "contents":"Description", "inputType":"", "class":"cl_td_nam" },
	                  	{"type":"td", "id":"td_01", "tarObj":"#tr_04", "contents":"", "inputType":"", "colspan":3},

                      {"type":"table", "id":"tb_02", "tarObj":"#article_02", "contents":"", "inputType":""},
                      {"type":"tr", "id":"tr_02_01", "tarObj":"#tb_02", "contents":"", "inputType":""},
                      	{"type":"td", "id":"td_01", "tarObj":"#tr_02_01", "contents":"req.No", "inputType":"", "class":"cl_td_nam" },
                      	{"type":"td", "id":"td_01", "tarObj":"#tr_02_01", "contents":"", "inputType":"", "class":"cl_td_val"},
                      	{"type":"td", "id":"td_01", "tarObj":"#tr_02_01", "contents":"doc.No", "inputType":"", "class":"cl_td_nam" },
                      	{"type":"td", "id":"td_01", "tarObj":"#tr_02_01", "contents":"", "inputType":"", "class":"cl_td_val"},
                      {"type":"tr", "id":"tr_02_02", "tarObj":"#tb_02", "contents":"", "inputType":""},
	                  	{"type":"td", "id":"td_01", "tarObj":"#tr_02_02", "contents":"posting Dat", "inputType":"", "class":"cl_td_nam" },
	                  	{"type":"td", "id":"td_02", "tarObj":"#tr_02_02", "contents":"", "inputType":""},
	                  		{"type":"td", "id":"td_inner_01", "tarObj":"#tb_02 #tr_02_02 #td_02", "contents":"", "inputType":"", "class":"cl_td_inner"},
	                  		{"type":"input", "id":"tf_01", "tarObj":"#tb_02 #tr_02_02 #td_02 #td_inner_01", "contents":"", "inputType":"text", "value":"2013.11.19"},
	                  	{"type":"td", "id":"td_01", "tarObj":"#tr_02_02", "contents":"doc Dat", "inputType":"", "class":"cl_td_nam" },
	                  	{"type":"td", "id":"td_01", "tarObj":"#tr_02_02", "contents":"", "inputType":""},
                      
	                  {"type":"tr", "id":"tr_02_03", "tarObj":"#tb_02", "contents":"", "inputType":""},
		              	{"type":"td", "id":"td_01", "tarObj":"#tr_02_03", "contents":"Currency", "inputType":"", "class":"cl_td_nam" },
	                  	{"type":"td", "id":"td_01", "tarObj":"#tr_02_03", "contents":"", "inputType":"", "colspan":3},
		                           
                      {"type":"tr", "id":"tr_02_04", "tarObj":"#tb_02", "contents":"", "inputType":""},
                      	{"type":"td", "id":"td_01", "tarObj":"#tr_02_04", "contents":"Description", "inputType":"", "class":"cl_td_nam" },
	                  	{"type":"td", "id":"td_01", "tarObj":"#tr_02_04", "contents":"", "inputType":"", "colspan":3},
	                  	
                     
                      
                      {"type":"button", "id":"bu_submit_gl", "tarObj":"#article_03", "contents":"Posting", "class":"bu_css2"},
                      {"type":"button", "id":"button_bu05", "tarObj":"#article_03", "contents":"clear", "class":"bu_css2"},
                      {"type":"button", "id":"bu_onInputProcessing", "tarObj":"#article_03", "contents":"OnInput", "class":"bu_css2"}
                     ];

var ar_sc_doc_fb51 = {"tarObj":"#div_wrapper", "objId":"nav_nav", "objects":ar_sc_doc_fb51_obj};



var ar_sc_login_obj = [
               {"type":"div", "id":"div_wrapper", "tarObj":"#id_body", "contents":""},
               {"type":"div", "id":"div_login_top", "tarObj":"#div_wrapper", "contents":"login_top"},
               {"type":"div", "id":"div_login", "tarObj":"#div_wrapper", "contents":"div_login1"},
               {"type":"div", "id":"div_login_box", "tarObj":"#div_login", "contents":"div_login_box"},
               {"type":"div", "id":"div_login_innerBox", "tarObj":"#div_login_box", "contents":""},
               {"type":"div", "id":"div_login_innerBox2", "tarObj":"#div_login_box", "contents":"Question:System Management (3174/3274/3244)"},
               {"type":"input", "id":"tf_01", "tarObj":"#div_login_innerBox", "contents":"<br>", "inputType":"text", "value":"oceanfog"},
               {"type":"input", "id":"tf_02", "tarObj":"#div_login_innerBox", "contents":"<br>", "inputType":"password", "value":"1123"},
               {"type":"button", "id":"bu_login", "tarObj":"#div_login_innerBox", "contents":"Login", "class":"bu_css2"},
              ];
var ar_sc_login = {"objects":ar_sc_login_obj};



/*****************************************
 * css object
 * 
 *****************************************/

var ar_css_docList = {
	"#div_wrapper":{ 
		//"background-color":"#ffffff"
		"background":"url("+gv_pageOption['rootDir']+"images/minimal-cartoon-clouds-blue-background-1024x768.jpg) no-repeat"
	   	},
};

var ar_css_home = {
	"#div_wrapper":{ 
		//"background-color":"#ffffff"
		"background":"url("+gv_pageOption['rootDir']+"images/minimal-cartoon-clouds-blue-background-1024x768.jpg) no-repeat"
	   	},		
}


var ar_css_fb51 ={"body":{ "background-color":"#ffffff"},
		"#div_wrapper":{ 
				//"background-color":"#ffffff"
			"background":"url("+gv_pageOption['rootDir']+"images/minimal-cartoon-clouds-blue-background-1024x768.jpg) no-repeat"
		
		   	},
		"table":{
			"display":"block",
			"padding":"0px",
			"min-width":"780px",
			"border":"0px solid #aaaaaa",
			"border-spacing":1,
			//"border-collapse":"collapse",
			"min-height":"50px"
		},
		"tr":{
			//"border":"1px",
			"border":"1px solid #aaaaaa",
			"height":"25px"
		},
		"td":{
			"border":"1px solid #aaaaaa",
			"margin":"0px"
		},
		".cl_td_nam":{
			"text-align":"center",
			"width":"120px",
			"background-color":"#639dc2"
		},
		".cl_td_val":{
			"width":"260px",
			"background-color":"#ffffff"
		},
		".cl_td_inner":{
			"border":"1px solid #aaaaaa",
			"width":"110px",
			"background-color":"#ffffff"
		},

		"input[type=text]":{ 
			"background-color":"#ffffff",
			"border":"none"
			},
		"input[type=password]":{ 
			"background-color":"#ffffff",
			"border":"none"
		},
		
		"#div_login_top":{ 
			"margin-top": "5%",
		    "margin-left": "auto",
		    "margin-right": "auto",
		    
			"width":"50%",
			"height":"30px",
			"background-color":"#ffffff",
			"border-style":"solid",
			"boarder-width":"1px",
			"border-radius":"5px",
			"border-color":"#bbbbbb"
			},

		"#div_login":{ 
				"margin-top": "1%",
			    "margin-left": "auto",
			    "margin-right": "auto",
			    
				"width":"50%",
				"height":"300px",
				"background-color":"#ffffff",
				"border":"1px solid #bbbbbb",
				"border-radius":"5px",
				},
		"#div_login_box":{ 
				"margin-top": "30px",
				"margin-left": "auto",
			    "margin-right": "auto",
			    
			    "paddin":"10px",
				
				"width":"85%",
				"height":"60%",
				"background-color":"#ffffff",
				"border-style":"solid",
				"boarder-width":"1px",
				"border-radius":"5px",
				"border-color":"#dddddd"
			},
		"#div_login_innerBox":{ 
				"margin-top": "10px",
				"margin-left": "auto",
			    "margin-right": "auto",
			    
			    "padding-top":"10px",
			    "padding-left":"30px",
				
				"width":"60%",
				"height":"80px",
				"background-color":"#ddddee",
				"border-style":"solid",
				"boarder-width":"1px",
				"border-radius":"5px",
				"border-color":"#dddddd"
			},
			
		"#div_login_innerBox2":{ 
				"margin-top": "10px",
				"margin-left": "auto",
			    "margin-right": "auto",
				
				"width":"90%",
				"height":"20px",
				"background-color":"#ddddee",
				"border-style":"solid",
				"boarder-width":"1px",
				"border-radius":"5px",
				"border-color":"#dddddd"
			},
			".bu_css":{ 
				"padding-left":"15px",
				"background-color":"#ffffff",
				"border-top":"1px hidden #dcdcdc",
				"border-right":"1px hidden #dcdcdc",
				"border-bottom":"1px solid #dcdcdc",
				"display":"inline-block",
				"color":"#222222",
				"font-size":"15px",
				"font-weight":"normal",
				"height":"30px",
				"line-height":"2px",
				"width":"180px",
				"text-align":"left"
				},
			".bu_css2":{
				
				"background-color":"#ffffff",
				"border":"1px solid #222222",
				"border-radius":"3px",
				"display":"inline-block",
				"color":"#111111",
				"font-size":"13px",
				"font-weight":"normal",
				"height":"20px",
				"line-height":"5px",
				"width":"50px",
				"text-align":"center"
				}
		};

var ar_css_login ={"body":{ "background-color":"#ffffff"},
		"#div_wrapper":{ 
				//"background-color":"#ffffff"
				"background":"url("+gv_pageOption['rootDir']+"./images/minimal-cartoon-clouds-blue-background-1024x768.jpg) no-repeat"
		   	},
		"input[type=text]":{ 
			"background-color":"#ffffff",
			"border":"none"
			},
		"input[type=password]":{ 
			"background-color":"#ffffff",
			"border":"none"
		},
		
		"#div_login_top":{ 
			"margin-top": "5%",
		    "margin-left": "auto",
		    "margin-right": "auto",
		    
			"width":"50%",
			"height":"30px",
			"background-color":"#ffffff",
			"border-style":"solid",
			"boarder-width":"1px",
			"border-radius":"5px",
			"border-color":"#bbbbbb"
			},

		"#div_login":{ 
				"margin-top": "1%",
			    "margin-left": "auto",
			    "margin-right": "auto",
			    
				"width":"50%",
				"height":"300px",
				"background-color":"#ffffff",
				"border-style":"solid",
				"boarder-width":"1px",
				"border-radius":"5px",
				"border-color":"#bbbbbb"
				},
		"#div_login_box":{ 
				"margin-top": "30px",
				"margin-left": "auto",
			    "margin-right": "auto",
			    
			    "paddin":"10px",
				
				"width":"85%",
				"height":"60%",
				"background-color":"#ffffff",
				"border-style":"solid",
				"boarder-width":"1px",
				"border-radius":"5px",
				"border-color":"#dddddd"
			},
		"#div_login_innerBox":{ 
				"margin-top": "10px",
				"margin-left": "auto",
			    "margin-right": "auto",
			    
			    "padding-top":"10px",
			    "padding-left":"30px",
				
				"width":"60%",
				"height":"80px",
				"background-color":"#ddddee",
				"border-style":"solid",
				"boarder-width":"1px",
				"border-radius":"5px",
				"border-color":"#dddddd"
			},
			
		"#div_login_innerBox2":{ 
				"margin-top": "10px",
				"margin-left": "auto",
			    "margin-right": "auto",
				
				"width":"90%",
				"height":"20px",
				"background-color":"#ddddee",
				"border-style":"solid",
				"boarder-width":"1px",
				"border-radius":"5px",
				"border-color":"#dddddd"
			}
		};


