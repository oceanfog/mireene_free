<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd"><html dir="ltr" lang="en"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"><title>Alert Page</title><link rel="stylesheet" href="../csstg.css" type="text/css"><style>
/* ---- ( iTunes CSS ) ---- */
table { 
	font: 80% Verdana, Arial, Helvetica, sans-serif;
	color: #000;
	text-align: left;
	border-collapse: collapse;
	border: 1px solid #666666;
	border-top: none;
}
table a {
	text-decoration: underline;
}
table a:visited {
	text-decoration: none;
}
tr.odd {
	background-color: #ebf3ff;
}
tr a {
	color: #000000;
}
tr:hover a {
	color: #ffffff;
}
tr:hover, tr.odd:hover {
	background-color: #3d80df;
	color: #ffffff;
}
caption {
	height: 45px;
	line-height: 44px;
	color: #60634E;
	font-weight: bold;
	text-align: center;
	margin: 0;
	padding: 0;
	margin-left: -1px;
	background: #ffffff url(image/captop.jpg) repeat-x;
	background-position: 50% top;
	border-left: 2px solid #616161;
	border-right: 2px solid #616161;
}
thead th {
	font-size: 10px;
	color: #000;
	background: #ffffff url(image/tbar.gif) repeat-x;
	height: 20px;
	text-align: center;
}
thead th:hover {
	background: #ffffff url(image/tbov.gif) repeat-x;
	vertical-align: middle;	
}
tr {
	vertical-align: top;
}
tr,th,td {
	padding: .75em;
	font-size: 10px;
}
td {
	border-left: 1px solid #dadada;
}
tfoot tr {
	background: #fff url(image/bbar.gif) repeat-x;
}
tfoot td, tfoot th{
	color: #000;
	border: 0px;
}
</style>


<table width="100%" align="left" summary="Submitted table designs" border="0">
  <thead>
    <tr>
      <th width="62">Severity</th>
      <th width="228">Alert Name</th>
      <th width="83"> DateTime</th>
      <th width="86">ServerName</th>
      <th width="449">Alert Description</th>
      <th width="84"> Summary</th>
    </tr>
  </thead>
</table>          
