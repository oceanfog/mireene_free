function getChartData(query){
	var chartData;
	jQuery.ajax({
		async: false,
		global: false,
		type: "get",
		url:"../php_exam/09twoArrayToJson.php",
		dataType:"json",
		data:{"query":query},
		success:function(data){
			chartData = data;
		},
		error:function(jqXHR){
			alert("error"+jqXHR);
			
		}
	});
	return chartData;
}


function testGetChartData(query){
	jQuery.ajax({
		type:"get",
		url:"../php_exam/09twoArrayToJson.php",
		dataType:"text",
		data:{"query":query},
		success:function(data){
			alert("success");
			console.log(data);
		},
		error:function(jqXHR){
			alert("error"+jqXHR);
		}
	});
}