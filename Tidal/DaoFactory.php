<?php
include 'Dao.php';

class DaoFactory
{
	public function getRainFallChartData( $resultSet  ){
		$resultArray = array();
		while( $rs = mysql_fetch_array( $resultSet ) ){
			$arrayMiddle = array("year"=>urlencode($rs['year']), "data"=>array());
			
			array_push($arrayMiddle['data'], (int)$rs['spring'] );
			array_push($arrayMiddle['data'], (int)$rs['summer'] );
			array_push($arrayMiddle['data'], (int)$rs['autumn'] );
			array_push($arrayMiddle['data'], (int)$rs['winter'] );
			
			array_push($resultArray, $arrayMiddle);
		}	
		return $resultArray;		
	}
	
	public function getRainFallChartDataHorizon( $resultSet ){
		$resultArray = Array();
		$arSpring = Array("season"=>"spring", "data"=>Array() );
		$arSummer = Array("season"=>"summer", "data"=>Array() );
		$arAutumn = Array("season"=>"autumn", "data"=>Array() );
		$arWinter = Array("season"=>"winter", "data"=>Array() );
		
		while( $rs = mysql_fetch_array( $resultSet ) ){
			array_push($arSpring['data'], (int)$rs['spring']);
			array_push($arSummer['data'], (int)$rs['summer']);
			array_push($arAutumn['data'], (int)$rs['autumn']);
			array_push($arWinter['data'], (int)$rs['winter']);
		}
		array_push($resultArray, $arSpring);
		array_push($resultArray, $arSummer);
		array_push($resultArray, $arAutumn);
		array_push($resultArray, $arWinter);
		
		return $resultArray;
	}
			
}

$getFnName=$_GET["name"];

$dao = new Dao();
$daoFactory = new DaoFactory();

$resultSet = $dao->getResultSet( $dao->getConnection(), "SELECT * FROM rainFall" );


print_r( urldecode( json_encode( $daoFactory->getRainFallChartDataHorizon( $resultSet ) ) ) );


	
?>