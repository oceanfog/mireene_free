<!-- 모바일 최근 리스트 : 메인 -->
<script type="text/javascript">
function file_download(link, file, no) {
    if (confirm("파일을 다운로드 하시겠습니까?"))
    if (0)
        win_open("../skin/board/mw.basic/mw.proc/download.popup.skin.php?bo_table=android_04&wr_id=62115&no="+no, "download_popup", "width=500,height=300,scrollbars=yes");
    else
        document.location.href=link;
}
</script>

<div id="home" class="current">
	<div class="toolbar">
		<h1>유니큐브 방송웹하드</h1>
		<a class="button slideup" id="infoButton" href="#search">검색</a>
	</div>
	<ul class="individual">
		<li>최근 방영 프로그램</li><li>채널</li>
	</ul>
	<ul class="rounded">
		<li class="forward"><a href="#list" class="pop">역전의 여왕 24회</a></li>
		<li class="forward">역전의 여왕 13회</li>
		<li class="forward">폭풍의 연인 2회</li>
		<li class="forward">욕망의 불꽃 12회</li>
	</ul>
	<ul class="individual">
		<li>테스트</li>
		<li><a href="http://uniqube.tv" target="_blank">Home</a></li>
	</ul>
	<div class="info">
		<p>ⓒUniqube</p>
	</div>
</div>

<!-- 모바일 리스트 : 다운로드 -->
<div id="list" class="current">
	<div class="toolbar">
		<a class="button back slideup" id="infoButton" href="#">뒤로</a>
		<h1>유니큐브 방송웹하드</h1>
		<a class="button slideup" id="infoButton" href="#search">검색</a>
	</div>
	<ul class="rounded">
		<li>역전의 여왕<br>MBC 월화드라마</li>
	</ul>
	<ul class="rounded">
		<li class="download"><a href="http://blumine.dyndns.org:8800/contents/main/file_download/1">24회(2011.01.04)</a></li>
		<li class="download">23회(2011.01.03)</li>
		<li class="download">22회(2011.01.03)</li>
		<li class="download">21회(2011.01.03)</li>
	</ul>
	<ul class="individual">
		<li>테스트</li>
		<li><a href="http://uniqube.tv" target="_blank">Home</a></li>
	</ul>
	<div class="info">
		<p>ⓒUniqube</p>
	</div>
</div>

<!-- 모바일 검색 -->
<div id="search" class="current">
	<form method="post" class="form" action="" id="search1">
	<div class="toolbar">
		<a class="button back slideup" id="infoButton" href="#">뒤로</a>
		<h1>유니큐브 방송웹하드</h1>
	</div>
	<ul class="rounded">
		<li><input type="text" name="q_word" placeholder="검색어를 입력하세요."></li>
	</ul>
	<a style="margin:0 10px;color:rgba(0,0,0,.9)" href="#" class="submit whiteButton">검색</a>
	</form>
	<br>
	<div class="info">
		<p>ⓒUniqube</p>
	</div>
</div>