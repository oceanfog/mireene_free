<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Webhard</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="description" content="MBC, webhard">
<meta name="keywords" content="webhard">
<link rel="stylesheet" href="<?=CSS_DIR?>/common.css" type="text/css">

<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js"></script>
<script type="text/javascript" src="<?=JS_DIR?>/common.js"></script>

</head>

<!--[if lt IE 7 ]> <body class="ie6"> <![endif]-->
<!--[if IE 7 ]> <body class="ie7"> <![endif]-->
<!--[if IE 8 ]> <body class="ie8"> <![endif]-->
<!--[if !IE]>--> <body> <!--<![endif]-->
<div id="wrap">
	<div id="wrapper">
		<!--header-->
		<div id="header" >
			<!--검색 영역-->
			<div id="search">
				 <a href="/contents/main"><span id="logo_text" class="logo">Uniqube Webhard</span></a>
				 <div class="mainsearch">
					<div class="poA">
						 <fieldset>
							<form method="post " action="">
								<input type="text"  class="maininput" /><input type="image"  src="<?php echo IMG_DIR;?>/inputbtn.gif"  class="maininputbtn"/>
							</form>
						 </fieldset>
					 </div>
					 <div class="Programwrap">
						<ul class="day_program">
							<li><a href="#">오프더 레코트 효리</a></li>
							<li><a href="#">연애 오락</a></li>
							<li><a href="#">오프더 레코트 효리</a></li>
							<li><a href="#">연애 오락</a></li>
						</ul>
					 </div>
				</div>
			</div>
			<!--//검색 영역-->
			<!--gnb 영역-->
			<div id="gnb">
				<div class="mainmenu" >
					<ul>
						<li><a href="#"><img src="<?php echo IMG_DIR;?>/menu3_1_on.gif"  alt="channel" /></a></li>
						<li><a href="#"><img src="<?php echo IMG_DIR;?>/menu3_1.gif"  alt="전체" /></a></li>
						<li><a href="#"><img src="<?php echo IMG_DIR;?>/menu1_1_on.gif"  alt="드라마" /></a></li>
						<li><a href="#"><img src="<?php echo IMG_DIR;?>/menu2_1.gif"  alt="예능" /></a></li>
						<li><a href="#"><img src="<?php echo IMG_DIR;?>/menu3_1.gif"  alt="음악" /></a></li>
					</ul>
				</div>
			</div>
			<!--//gnb 영역-->
		</div>
		<!--//header-->
		<div id="container" >
		<div id="content">

		<div class="Con_cont" >
			<div class="Con_left">
				<div  class="time_con" style="margin-top:0px; margin-left:7px; _margin-left:4px; color:#ffffff; font-size:16px; height:80px;">
				로그인 박스
				</div>
				<!--notice-->
				<ul class="Red_line" style="width:225px;">
					<li style="margin-top:0px; margin-left:7px; _margin-left:4px; color:#ffffff; font-size:16px;">공지사항</li>
				</ul>
				<div  class="time_con">
					<ul class="list_list">
						<li class="bg">
							<span class="bg_date">2010.09.30</span>
							<span class="bg_tx">새로운 동영상이 업로드</span>
						</li>
						<li>
							<span class="bg_date">2010.09.30</span>
							<span class="bg_tx">새로운 동영상이 업로드</span>
						</li>
						<li class="bg">
							<span class="bg_date">2010.09.30</span>
							<span class="bg_tx">새로운 동영상이 업로드</span>
						</li>
						<li>
							<span class="bg_date">2010.09.30</span>
							<span class="bg_tx">새로운 동영상이 업로드</span>
						</li><li class="bg">
							<span class="bg_date">2010.09.30</span>
							<span class="bg_tx">새로운 동영상이 업로드</span>
						</li>
					</ul>
				</div>
				<!--notice-->
				<!--banner-->
				<div class="banner_zone">
					<a href="#"><img src="<?php echo IMG_DIR;?>/banner_img1.gif"  alt="" /></a>
				</div>
				<!--//banner-->
			</div>