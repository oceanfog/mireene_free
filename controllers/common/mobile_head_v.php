<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<title>Uniqube Ad Mobile V0.1</title>
	<!-- /themes/apple/theme.min.css 아이폰테마 -->
	<style type="text/css" media="screen">@import "<?php echo JS_DIR; ?>/jqtouch/jqtouch/jqtouch.min.css";</style>
	<style type="text/css" media="screen">@import "<?php echo JS_DIR; ?>/jqtouch/themes/jqt/theme.css";</style>
	<script src="<?php echo JS_DIR; ?>/jqtouch/jqtouch/jquery.1.3.2.min.js" type="text/javascript" charset="utf-8"></script>
	<script src="<?php echo JS_DIR; ?>/jqtouch/jqtouch/jqtouch.min.js" type="application/x-javascript" charset="utf-8"></script>
	<script type="text/javascript" charset="utf-8">
		var jQT = new $.jQTouch({
			icon: 'jqtouch.png',
			addGlossToIcon: false,
			startupScreen: 'jqt_startup.png',
			statusBar: 'default',
			preloadImages: [
				'<?php echo JS_DIR; ?>/jqtouch/themes/jqt/img/back_button.png',
				'<?php echo JS_DIR; ?>/jqtouch/themes/jqt/img/back_button_clicked.png',
				'<?php echo JS_DIR; ?>/jqtouch/themes/jqt/img/button_clicked.png',
				'<?php echo JS_DIR; ?>/jqtouch/themes/jqt/img/grayButton.png',
				'<?php echo JS_DIR; ?>/jqtouch/themes/jqt/img/whiteButton.png',
				'<?php echo JS_DIR; ?>/jqtouch/themes/jqt/img/loading.gif'
				]
		});
		// Some sample Javascript functions:
		$(function(){
			// Show a swipe event on swipe test
			$('#swipeme').swipe(function(evt, data) {
				$(this).html('You swiped <strong>' + data.direction + '</strong>!');
			});
			$('a[target="_blank"]').click(function() {
				if (confirm('This link opens in a new window.')) {
					return true;
				} else {
					$(this).removeClass('active');
					return false;
				}
			});
			// Page animation callback events
			$('#pageevents').
				bind('pageAnimationStart', function(e, info){
					$(this).find('.info').append('Started animating ' + info.direction + '&hellip; ');
				}).
				bind('pageAnimationEnd', function(e, info){
					$(this).find('.info').append(' finished animating ' + info.direction + '.<br /><br />');
				});
			// Page animations end with AJAX callback event, example 1 (load remote HTML only first time)
			$('#callback').bind('pageAnimationEnd', function(e, info){
				if (!$(this).data('loaded')) {                      // Make sure the data hasn't already been loaded (we'll set 'loaded' to true a couple lines further down)
					$(this).append($('<div>Loading</div>').         // Append a placeholder in case the remote HTML takes its sweet time making it back
						load('ajax.html .info', function() {        // Overwrite the "Loading" placeholder text with the remote HTML
							$(this).parent().data('loaded', true);  // Set the 'loaded' var to true so we know not to re-load the HTML next time the #callback div animation ends
						}));
				}
			});
			// Orientation callback event
			$('body').bind('turn', function(e, data){
				$('#orient').html('Orientation: ' + data.orientation);
			});
		});
		function scrapTwitter() {
			var content = '"' + document.title.replace(' - 세상을 보는 눈, 글로벌 미디어 - 세계일보 -','').substring(0,40) + ' 수애의 우아한 니킥!": ' + 'http://bit.ly/i4iEVr';
			window.open('http://twitter.com/home?status='+encodeURIComponent(content),'','','');
		}


		function vidEvent() {
		var videos = document.getElementsByTagName('video');
		var vidCount = videos.length;
		for(i=0;i<vidCount;i++) {
				videos[i].addEventListener('click',bang,false);
			}
		}
		function bang() { this.play(); }
		window.onload = vidEvent;

	</script>
	<style type="text/css" media="screen">
		body.fullscreen #home .info {
			display: none;
		}
		#about {
			padding: 100px 10px 40px;
			text-shadow: rgba(255, 255, 255, 0.3) 0px -1px 0;
			font-size: 13px;
			text-align: center;
			background: #161618;
		}
		#about p {
			margin-bottom: 8px;
		}
		#about a {
			color: #fff;
			font-weight: bold;
			text-decoration: none;
		}
		#layer1{
        width:100%; height:100%;
        position:relative;

        }
		#layer2{
        width:210px; height:44px;

        left:10px; top:10px;      /*** 이 위치는 상위 레이어(layer)를 기준으로 한 위치입니다 ***/
		position:absolute;
        /* background-image:url('http://blumine.iptime.org:8800/mobile/hutLogo01.png'); */
		z-index:1;
		visibility: visible;
		}
	</style>
</head>
<body>