<?php

class Main extends Controller {

	function Main()
	{
		parent::Controller();
		$this->load->model('contents_m', 'cm');
	}

	function _remap($method) {
		//접근 브라우저에 따른 분리
		if( BROWSER_TYPE == 'W' )
		{
			//웹인 경우
			$this->load->view('common/head_v');
			$this->{$method}();
			$this->load->view('common/foot_v');
		}
		else if( BROWSER_TYPE == 'M' )
		{
			//모바일인 경우
			$this->load->view('common/mobile_head_v');
			$this->{$method}();
			$this->load->view('common/mobile_foot_v');
		}
	}

	/**
	* 메인 페이지
	*
	*/
	function index()
	{
		if( BROWSER_TYPE == 'W' )
		{
			//웹인 경우
			$this->load->view('main/main_v');
		}
		else if( BROWSER_TYPE == 'M' )
		{
			//모바일일 경우
			$this->load->view('main/m_recent_v');
		}
	}
}
?>