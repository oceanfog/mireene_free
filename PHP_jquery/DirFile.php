<?php
header("Content-Type: text/html; charset=utf-8");

class DirFile
{
	function getFoldersFiles($folderName){
		chdir("/web/home/oceanfog3/html/");
		
		if($folderName ==""){
			$dir = "./";	
		}else{
			$dir = "./$folderName/";
		}
		
		$ar_dir = Array( 'folders' => Array(), 'files' => Array() );
		// Open a known directory, and proceed to read its contents
		if (is_dir($dir)) {
	    	if ($dh = opendir($dir)) {
		        while (($file = readdir($dh)) !== false) {
					if( filetype($dir.$file) == "dir"){
						if ($file == "." || $file == "..") continue;				
						array_push($ar_dir['folders'], $file);
					}else{
						array_push($ar_dir['files'], $file);
					}
		        }
	        closedir($dh);
			sort($ar_dir['folders']);
			sort($ar_dir['files']);
			
	  		}
		}
		return $ar_dir;	
	}
}

?>