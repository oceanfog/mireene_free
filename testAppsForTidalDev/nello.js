function getChartData(){	
	var chartDataJson ; 
	jQuery.ajax({
		'type': "GET",
		'async': false,
		'global': false,
		'url': "TidalDaoFactory.php",
		'dataType': "json",
		'success': function (data) {
			chartDataJson = data;
		},
		'error':function(jqXHR){
			alert("error" + jqXHR);
			jQuery.each(jqXHR, function(key, value){
				console.log(key + value);
			});
		},
		'beforeSend':function(){
			//alert("hello");
		},
		'data':{sql:"select * from table", toTime:"2013-05-10 15:48" }
	});
	
return chartDataJson;
}


function drawingFirstChart(){
	chartData2 = [{"name":"DBRequest",    "data":[10,20,30,345,37,89,79]},
	              {"name":"ResponseTime", "data":[60,80,100,65,37,90,21]},
	              {"name":"FrespTime",    "data":[70,40,200,34,45,87,11]},
				 ];
	
	chartData3 = getChartData();
	
	
	setOptions(chartData, "div01");
	options.chart.type = 'column';
	options.legend.enabled = true;
	//options.chart.inverted = true;
	options.title.text = '데이터 트래픽 추이';
	//options.chart.marginLeft = 150;
	//options.chart.spacingLeft = 150;
	chart1 = new Highcharts.Chart(options);
	
	setOptions(chartData3, "div02");
	options.chart.type = 'column';
	options.plotOptions.series.stacking = '';
	chart2 = new Highcharts.Chart(options);
		
	setOptions(chartData3, "div03");
	options.chart.type = 'bar';
	options.legend.enabled = true;
	options.plotOptions.series.stacking = '';
	chart1 = new Highcharts.Chart(options);
	
	setOptions(chartData, "div04");
	options.chart.type = 'area';
	chart2 = new Highcharts.Chart(options);
			
}

var chartData = [{"name":"krk1","data":[10,20,30,40,50,70]},
             {"name":"krk2","data":[20,30,40,50,60]},
             {"name":"krk3","data":[30,40,50,60,70]},
             {"name":"krk4","data":[30,10,50,90,30]},
             {"name":"krk5","data":[10,59,60,90,30]}
             ];


function setOptions(cData, targetObject){
	options = {
		chart: {
			renderTo: targetObject,
			type: 'column',
			marginRight: 10,
			marginBottom: 20,
			width: 490,
			height: 320
		},
		colors: [
		         '#2f7ed8', 
		         '#0d233a', 
		         '#8bbc21', 
		         '#910000', 
		         '#1aadce', 
		         '#492970',
		         '#f28f43', 
		         '#77a1e5', 
		         '#c42525', 
		         '#a6c96a'
		      ],
		 loading: {
            hideDuration: 1000,
            showDuration: 1000
        },
		plotOptions: {
    		series: {
    	    	stacking: 'normal'
 	   		}
		},

		legend: {
			enabled:false
		},
		title: {
            text: 'My custom title'  
        },
		
		xAxis: {categories: ['03년', '04년', '05년', '06년', '07년','08년','09년','10년','11년','12년']    },
		//xAxis: {categories: ['spring', 'summer', 'autumn', 'winter']    },
		//yAxis: {categories: ['spring', 'summer', 'autumn', 'winter']    },
		series:cData
	}
}
