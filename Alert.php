<style>
/* ---- ( iTunes CSS ) ---- */
table {
	color: #000;
	text-align: left;
	border-collapse: collapse;
	border: 1px solid #666666;
	border-top: none;
	font-family: Verdana, Geneva, sans-serif;
	font-size: 9px;
	position: absolute;
	top: 0px;
	left: 0px;
	width: 1280px;
}
table a {
	text-decoration: underline;
}
table a:visited {
	text-decoration: none;
}
tr.odd {
	background-color: #ebf3ff;
}
tr a {
	color: #000000;
}
tr:hover a {
	color: #ffffff;
}
tr:hover, tr.odd:hover {
	background-color: #3d80df;
	color: #ffffff;
}
caption {
	height: 45px;
	line-height: 44px;
	color: #60634E;
	font-weight: bold;
	text-align: center;
	width: 100%;
	margin: 0;
	padding: 0;
	margin-left: -1px;
	background: #ffffff url(captop.jpg) repeat-x;
	background-position: 50% top;
	border-left: 2px solid #616161;
	border-right: 2px solid #616161;
}
thead th {
	font-size: 10px;
	color: #000;
	background: #ffffff url(tbar.gif) repeat-x;
	height: 33px;
	text-align: center;
}
thead th:hover {
	background: #ffffff url(tbov.gif) repeat-x;
	
}
tr {
	vertical-align: top;
}
tr,th,td {
	padding: .75em;
	font-size: 10px;
}
td {
	border-left: 1px solid #dadada;
}
tfoot tr {
	background: #fff url(bbar.gif) repeat-x;
}
tfoot td, tfoot th{
	color: #000;
	border: 0px;
}
</style></head>


<script type="text/javascript"> 
	function OpenNewWindow(url) 
	{ 
		window.open(url,'','left=0, top=0, toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=no, width=1024 height=768');
	 } 
</script> 



<body>

<table summary="Submitted table designs">
<?
require "Mylib.php";

$Myclass = new Myclass;

$conn=OpenDB();

///////////////////////////////////
// * 경고관리 페이지 생성
///////////////////////////////////

// Date Calc : strtotime("+1 week 2 days 4 hours 2 seconds")

$sql = $Myclass->GetQuery_AlertData(date("Y-m-d H:i:s", strtotime("-10 hours")),date("Y-m-d H:i:s"));


//echo "<br> [Query] <br><br>";
//echo $sql;

//$sql=str_replace("select","select top 10",$sql); // 상위10개만 짤라서 메인화면에 출력
$ResultSet=ExecuteQuery($sql);

//echo "<br><br> [Data] <br><br>";

?>

          
<?       
  
echo " <tbody> ";

$count = 0; // 짝수라인에는 하늘색 배경으로 출력 <tr class='odd'> 추가
$Severity = '';
          
while ($row = mssql_fetch_array($ResultSet)) { //패치할때는 colum명 대소문자 구분

//$ = ($count + 1)/2 ;		

/*
		// 1. Severity
	    echo $row[Severity] . "<br>";
		
		// 2. ProcessName
   	   	$ProcessName = $Myclass->Get_ProcessName($row[ProcessNameId]);
		echo $ProcessName . "<br>";			 
		
		// 3. CreatedTime
		echo $row[CreatedTime] . "<br>";

		// 4. ServerName
   		echo $row[ConfigurationItemId] . "<br>";

		// 5. Description
 		echo $row[Description] . "<br>";

		// 6. AutomationSummary URL
		echo $row[AutomationSummary] . "<br><br>";

*/
	$count = $count + 1;
	$mod = $count%2;


	if ($mod == 1) {
		$TR = "<TR>"; // even line
	 } else {
	 	$TR = "<TR class='odd'>"; // odd line
	 };
	
	
	echo $TR;
	
		switch ($row[Severity])
		{
		case '1';
			$Severity = "<img src='image/blue.png' width='20' height='20'></img>";
			break;
		
		case '2';
			$Severity = "<img src='image/yellow.png' width='20' height='20'></img>";
			break;
	
		case '3';
			$Severity = "<img src='image/red.png' width='20' height='20'></img>";
			break;

		default;
			break;
		};

	echo "<TH align='center'>". $Severity ."</TH>";

	$ProcessName = $Myclass->Get_ProcessName($row[ProcessNameId]);
	echo "<TD width='300' align='center'>". $ProcessName ."</TD>";
	
	echo "<TD width='100' align='center'>". $row[CreatedTime] ."</TD>";
	
	$ObjectName = $Myclass->Get_ObjectName($row[ConfigurationItemId]);
	echo "<TD width='100' align='center'>". $ObjectName ."</TD>";
	
	echo "<TD width='605'>". substr($row[Description],0,75) ."</TD>";
	
	echo "<TD width='100' align='center'>" . 
		"<A href = javascript:OpenNewWindow('" . str_replace('localhost', TEOSERVER_IP, $row[AutomationSummary]) . "')>" . 
		 "<img src = 'image/find.png' border='0' width='15' height='15'>" .
		 "</A></TD>";
	
	echo "</TR>";


} // end fetch


CloseDB($conn);

?>