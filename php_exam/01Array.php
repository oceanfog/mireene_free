<?php


$array = array(
    1    => "a",
    "1"  => "b",
    1.5  => "c",
    true => "d",
);
var_dump($array);

echo "<br>";
echo $array;
echo "<br>";
echo $array[1];



$array2 = array(
    "foo" => "bar",
    "bar" => "foo",
);

echo "<br>";
echo $array2[foo]."<br>";


$array3 = array(
"a", "b", "c", array("l", "m", "n")
);

var_dump($array3);
echo "<br><br>";

$array4 = array(
"items" =>"a",
"b", "c", "item" => array("title" => "l", "m", array("hello2") )
);

echo "items: ".$array4[item][title];
var_dump($array4[item][1]);
//var_dump($array4);

echo "<br><br>";
echo "------------";

$array5 = array();

$array5["folder"] = "hello";

array_push($array5, "hello" );

print_r($array5);

array_push($array5[hello], "nello");



?>