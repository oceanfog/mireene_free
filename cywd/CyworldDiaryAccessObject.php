<?php

header("Content-Type: text/html; charset=UTF-8");

class CyworldDiaryAccessObject{
	function getDiaryList($appKey, $access_token, $urlWithParameter ){
			$header[] = "Accept: application/json";
			$header[] = "Content-Type: application/xml; charset=utf-8";
			$header[] = "Accept-Language: ko_KR";
			$header[] = "access_token: $access_token";
			$header[] = "appKey: $appKey";
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $urlWithParameter);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1000);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
			$g = curl_exec($ch);
			curl_close($ch);
			$gotData = json_decode($g,true);

		return $gotData;
	}
	
	function printDiaryList($gotData){
			$item = $gotData[items][item];
			for ($i = 0; $i < sizeof($item); $i++) {
				echo $i."_<a href=#>".$item[$i][itemDate]."</a>".$item[$i][title]." "."<br>";
			}
	}
}

?>