/**
 * @author KyoungRock
 */
var date = new Date();

//document ready시점에서 싸이월드 다이어리 목록을 조회해서 target_div에 출력한다.
jQuery(document).ready(function() {
	//print list
	setDiaryList(jQuery(tf_01).val(), 1);

	//print date in tf_date
	//jQuery(tf_date).val( date.getFullYear() +""+ date.getMonth() +""+ date.getDate() );

	jQuery(bu_submit).click(function() {
		setDiaryList(jQuery(tf_01).val(), 1);
	});

	jQuery(sel02).change(function() {
		setDiaryList(jQuery(tf_01).val(), jQuery(this).val());
	});

});

function setDiaryList(accessToken, page) {
	jQuery.ajax({
		url : "cDaoFactory.php",
		data : {
			accessToken : accessToken,
			page : page
		},
		type : "get",
		dataType : "html",
		timeout : 30000,
		success : function(data) {
			jQuery('div#target_div').html("<div>" + data + "</div>");
		},

		error : function(xhr, textStatus, errorThrown) {
			jQuery('div#target_div').html("<div>" + textStatus + "(HTTP-" + xhr.status + "/" + errorThrown + ")</div>");
		}
	});
}


jQuery(document).ajaxComplete(function() {
	//target_div에 출력된 싸이월드 다이어리의 title 옆에 있는 date를 클릭하면 해당 글의 내용이 target_contents에 출력된다.
	jQuery('div#target_div a').click(function() {
		at = jQuery(tf_01).val();
		date = jQuery(this).html().replace(".", "").replace(".", "");

		jQuery.ajax({
			url : "viewDiaryAboutDate.php",
			data : {
				accessToken : at,
				date : date
			},
			type : "get",
			dataType : "html",
			timeout : 30000,
			success : function(data) {
				jQuery('article#target_article1').html("<div>" + data + "</div>");
			},

			error : function(xhr, textStatus, errorThrown) {
				jQuery('div#target_contents').html("<div>" + textStatus + "(HTTP-" + xhr.status + "/" + errorThrown + ")</div>");
			}
		});
		/*ajax end */
	});
	/*.click end */

	
	//write diary button click
	jQuery(bu_content_summit).click(function() {
		at = jQuery(tf_01).val();
		date = jQuery(tf_date).val();
		content = jQuery('textarea#ta_content').val();

		alert(content);

		jQuery.ajax({
			url : "writeDiary.php",
			data : {
				accessToken : at,
				date : date,
				content : content
			},
			type : "get",
			dataType : "html",
			timeout : 30000,
			success : function(data) {
				jQuery('div#writtenStatus').html(data);
				ajax(jQuery(tf_01).val(), 1);
			},

			error : function(xhr, textStatus, errorThrown) {
				jQuery('div#writtenStatus').html("<div>" + textStatus + "(HTTP-" + xhr.status + "/" + errorThrown + ")</div>");
			}
		});
		/*.ajax end */
	});
	/*.click end */


});
/*.ajaxComplete end*/
