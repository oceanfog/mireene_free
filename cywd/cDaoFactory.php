<?php
//include Class Php file
include 'CyworldDiaryAccessObject.php';

//get parameter using get method.
$page=$_GET["page"];
$access_token =$_GET["accessToken"];

$appKey="5414ffe8-95cd-3bcd-a0e7-bca5470d4530";
$urlWithParameter="https://apis.skplanetx.com/cyworld/minihome/me/diaries/2/items?version=1&page=$page&count=50&searchType=0&searchKeyword=";

//define Class from included file
$cdao = new CyworldDiaryAccessObject();

//running method from defined class 'cdao'
$array_diary_list = $cdao->getDiaryList($appKey, $access_token, $urlWithParameter);

//running print method
$cdao->printDiaryList($array_diary_list);


?>