<?php

	/**
	 * 티스토리로 부터 받은 code와 함께 access_token 요청을 합니다.
	 */
	$authorization_code = $_REQUEST['code'];
	
	$client_id = 'a3b2b3d11f97020c76224d3fc5e2087e';
	$client_secret = '53693aa1b5043741794f1a15d936afd0df4e2010f625726078436bc8d5d8dd487dc40e98';
	$redirect_uri = 'http://oceanfog3.mireene.com/tistoryApiTest/oauth_callback.php';
	$grant_type = 'authorization_code';   //반드시 이단계에서는 authorization_code 라고 입력

	$url = 'https://www.tistory.com/oauth/access_token/?code=' . $authorization_code . '&client_id=' . $client_id . 
			'&client_secret=' . $client_secret . '&redirect_uri=' . urlencode($redirect_uri) . '&grant_type=' . $grant_type;

	$access_token = file_get_contents($url);
	echo $access_token;
	
?>