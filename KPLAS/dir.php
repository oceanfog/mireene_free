<?php

$dirname = "../jqueryM/";
$counter_file = 0;
$counter_dir = 0;

if (!$dh = @opendir($dirname)) {
     fwrite(STDERR, "에러: 디렉토리를 열 수 없습니다.");
     exit(1);
}


while (($file = readdir($dh)) !== false) {
	if ($file == "." || $file == "..") continue; // . 과 .. 디렉토리는 무시
    echo "$file<br>";
    $file_full_path = $dirname.DIRECTORY_SEPARATOR.$file;
    if (is_file($file_full_path)) $counter_file++;
    if (is_dir($file_full_path)) $counter_dir++;
}

  printf("\n\n\t[ %d 개의 파일 / %d 개의 디렉토리 ]\n", $counter_file, $counter_dir);
  closedir($dh);

?>